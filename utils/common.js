const fs = require('fs');
const crypto = require('crypto');
const moment = require('moment');
const globalContext = require('../include/global');
let MODULE = {

    md5: function (str) {
        let md5 = crypto.createHash('md5');
        return md5.update(str).digest('hex');
    },

    sha256: function (str) {
        let sha256 = crypto.createHash('sha256');
        return sha256.update(str).digest("hex");
    },

    sha1: function (str) {
        let sha1 = crypto.createHash('sha1');
        return sha1.update(str).digest("hex");
    },

    isnull: function (obj) {
        if (typeof obj === 'undefined' || obj === null) {
            return true;
        } else {
            return false;
        }
    },

    //先序
    recursivePreExecInObj: function (obj, handler, param) {
        if (typeof obj != 'object') {
            return;
        }
        if (obj instanceof Array) {
            for (let j = 0; j < obj.length; j++) {
                MODULE.recursivePreExecInObj(obj[j], handler, param);
            }
        } else {
            let ret = handler(obj, param);
            if (ret) {
                param = ret;
            }
            let keys = Object.keys(obj);
            for (let i = 0; i < keys.length; i++) {
                if (typeof obj[keys[i]] === 'object') {
                    MODULE.recursivePreExecInObj(obj[keys[i]], handler, param);
                }
            }
        }
    },

    //后序
    recursivePostExecInObj: function (obj, handler, param) {
        if (typeof obj != 'object') {
            return;
        }
        if (obj instanceof Array) {
            for (let j = 0; j < obj.length; j++) {
                MODULE.recursivePostExecInObj(obj[j], handler, param);
            }
        } else {
            let keys = Object.keys(obj);
            for (let i = 0; i < keys.length; i++) {
                if (typeof obj[keys[i]] === 'object') {
                    MODULE.recursivePostExecInObj(obj[keys[i]], handler, param);
                }
            }
            handler(obj, param);
        }
    },

    delKeyInObj: function (obj, key) {
        MODULE.recursivePreExecInObj(obj, function (o) {
            delete o[key];
        });
    },

    timestampNow: function () {
        return Math.ceil(new Date().getTime() / 1000);
    },

    checkFieldExists: function (obj, fName, noException) {
        if (typeof obj[fName] == 'undefined' || obj[fName] == null) {
            if (!noException) {
                throw {
                    what: -1,
                    message: '缺少必填信息 ' + fName,
                };
            } else {
                return false;
            }
        }
        return true;
    },

    checkFieldIsArray: function (obj, fName, noException) {
        let bRes;

        if (typeof Array.isArray === "function") {
            bRes = Array.isArray(obj[fName]);
        } else {
            bRes = Object.prototype.toString.call(obj[fName]) === "[object Array]";
        }

        if (!noException && !bRes) {
            throw {
                what: -1,
                message: '输入参数 ' + fName + ' 不是数组',
            };
        }

        return bRes;
    },

    checkFieldIsDate: function (obj, fName, noException) {
        let dateValid = moment(obj[fName]).isValid();
        if (!dateValid) {
            if (!noException) {
                throw {
                    what: -1,
                    message: '错误的时间格式 ' + obj[fName],
                };
            } else {
                return false;
            }
        }
        return true;
    },
    getRandomNum: function (Min, Max) {
        var Range = Max - Min;
        var Rand = Math.random();
        return (Min + Math.round(Rand * Range));
    },
    keysort:function (key,sortType){
        return function(a,b){
            return sortType ?~~(a[key]<b[key]):~~(a[key]>b[key])
        }
    },
    getModel:function(model) {
        return globalContext.database['mysql']['db_muller'].import( '../include/models/' + model);
    },
    delay:function(ms){
        return new Promise(function(resolve,reject){
            setTimeout(resolve,ms)

        })
    }
}

module.exports = MODULE;
