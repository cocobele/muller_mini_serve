
class Logger {

    constructor(req){
        if(req){
            this.requestId = req['request_id']
        } else {
            this.requestId = "no_request_id"
        }
    }

    debug(msg){
        this.stdout({
            'logger.debug': msg,requestId:this.requestId
        })
    }

    info(msg){
        this.stdout({
            'logger.info': msg,requestId:this.requestId
        })
    }

    error(msg){
        this.stdout({
            'logger.error': msg,requestId:this.requestId
        })
    }

    printExecption(e){
        this.error(e.toString());
        if(e.stack){
            this.error(e.stack);
        }

    }

    stdout(msgObj){
        console.log(JSON.stringify(msgObj));
    }

}
var logger = new Logger();


module.exports = {

    getReqLogger:function(req){
        let reqLogger =  new Logger(req)
        return reqLogger
    },

    error: function(msg){
        logger.error(msg);
    },

    debug: function(msg){
        logger.debug(msg);
    },

    info: function(msg){
        logger.info(msg);
    },

    printExecption: function(e){
        logger.error(e.toString());
        if(e.stack){
            logger.error(e.stack);
        }
    },
    
};
