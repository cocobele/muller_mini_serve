/***
 *
 *
 *
 *
 */

const dataHelper = require("./data")
const moment = require("moment")
const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const wx = require("./wx")
let globalContext = require(APP_BASE_PATH+'/include/global');
const cache = require(APP_BASE_PATH + "include/cache")
module.exports = {
    CreateOrder,
    getB2cOrderDetail,
    getB2cOrderList,
    RepayOrder,
    getShoppingcartDetail
}


async function RepayOrder(req,res){


    let logger = req.logger
    let param = req.body
    try {
        cu.checkFieldExists(param, 'id');
    } catch (e) {
        logger.printExecption(e);
        return res.json({
            ret_code: ec.PARAM_ERROR.code,
            ret_msg: e.message || ec.PARAM_ERROR.message,
        });
    }


    let whereRule = {};

    whereRule['$t_b2c_order.id$'] = {
        '$eq': param['id'],
    }
    whereRule['$t_b2c_order.lstate$'] = {
        '$eq': enums.DataLState.ACTIVE,
    }
    //todo 目前只支持 未支付的订单
    whereRule['$t_b2c_order.trade_state$'] = {
        '$in': [enums.OrderTradeState.NotPay],
    }
    whereRule['$t_b2c_order.openid$'] = {
        '$eq': req.session['openId']
    }

    const B2COrder = cu.getModel('b2c_order');


    let queryParam = {
        attributes: [
            'id',
            'commodity_name',
            'total_fee',
            'cash_fee',
            'num',
            'time_start',
            'time_pay',
            'time_end',
            'trade_state',
            'trade_state_desc',
            'remark',
            'contact_name',
            'contact_phone',
            'pay_channel_type',
            'modifier',
            'modify_time',
            'create_time',
        ],
    }
    queryParam['where'] = whereRule;
    try {
        let order = await B2COrder.findAll(queryParam);
        order = JSON.parse(JSON.stringify(order));

        if (order.length < 1) {
            return res.json({
                ret_code: ec.RES_NOT_EXISTS.code,
                ret_msg: ec.RES_NOT_EXISTS.message,
            });
        }

        let data = order[0];
        let wxOpts = {
            body: data['commodity_name'],
            out_trade_no: data['id'],
            fee_type: data['cash_fee_type'],
            total_fee: data['cash_fee'],
            spbill_create_ip: '127.0.0.1',
            notify_url: globalContext.config['wx']['pay']['nofityUrl'],
            trade_type: 'JSAPI',
            openid:req.session['openId']
        }

        let result = await wx.Unifiedorder(wxOpts)
        logger.info('wx.Unifiedorder',result)
        if (result['return_code'] != "SUCCESS" || result['result_code'] != "SUCCESS") {
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
        let prepay_id = result['prepay_id']

        let resOpt = {
            package:`prepay_id=${prepay_id}`,
            appId:globalContext.config['wx']['login']['appId'],
            nonceStr:`${new Date().getTime()}${Math.ceil(Math.random()*100)}`,
            signType:'MD5',
            timeStamp:new Date().getTime()
        }
        return res.json({
            ret_code: ec.SUCCESS.code,
            ret_msg: ec.SUCCESS.message,
            ...wx.getAppSign(resOpt),
            order_id: data['id']
        });
    } catch (e) {
        logger.printExecption(e);
        return res.json({
            ret_code: ec.FAILED.code,
            ret_msg: ec.FAILED.message,
        });
    }


}


/**
 * 小程序提交订单
 * @param {*} req
 * @param {*} res
 */
async function CreateOrder(req, res) {
    let logger = req.logger
    let param = req.body
    try {
        // cu.checkFieldExists(param, 'num');
        // cu.checkFieldExists(param, 'stock_id');
        cu.checkFieldExists(param, 'order');
        cu.checkFieldExists(param, 'contact_name');
        cu.checkFieldExists(param, 'contact_addr');
        cu.checkFieldExists(param, 'contact_phone');
        cu.checkFieldExists(param, 'pay_channel_type');
    } catch (e) {
        logger.printExecption(e);
        return res.json({
            ret_code: ec.PARAM_ERROR.code,
            ret_msg: e.message || ec.PARAM_ERROR.message,
        });
    }
    let stock_id_array=[];
    let orderMap={};
    let totalfee=0;
    param.order.map((item)=>{
        stock_id_array.push(item.stock_id)
        orderMap[item.stock_id]=item.num
        totalfee+=item.num*item.fee
    })

    const CommodityStock = cu.getModel('commodity_stock')
    const ArtWork = cu.getModel('artwork')
    const Product = cu.getModel('product')
    const Order = cu.getModel('b2c_order')
    const OrderStock = cu.getModel('order_stock')


    let whereRule = {};
    whereRule['$t_commodity_stock.id$'] = {
        '$in': stock_id_array,
    }

    whereRule['$t_commodity_stock.lstate$'] = {
        '$eq': enums.DataLState.ACTIVE,
    }

    let queryParam = {
        attributes: [
            'id',
            'fee',
            'fee_type',
            'num',
            'spec_name',
            'commodity_id',
            'commodity_type'
        ],
    }

    queryParam['where'] = whereRule;

    let stocks = await CommodityStock.findAll(queryParam);
    stocks = JSON.parse(JSON.stringify(stocks));

    if (stocks.length < param.order.length) {
        return res.json({
            ret_code: ec.RES_NOT_EXISTS.code,
            ret_msg: ec.RES_NOT_EXISTS.message,
        });
    }
    stocks.map((stock)=>{
        //检查库存
        console.log(stock)
        if (stock.num < orderMap[stock.id]['num']) {
            return res.json({
                ret_code: ec.STOCK_QUANTITY_UNAVAILABLE.code,
                ret_msg: ec.STOCK_QUANTITY_UNAVAILABLE.message,
            });
        }



    })

 //  return res.json({res:stocks,totalfee:totalfee});


/*
    let commodity = {}
    if (stock['commodity_type'] == enums.CommodityType.ArtWork) {
        commodity = await ArtWork.findAll({
            attributes: ["name","preview_pic_url"],
            where: {
                "id": stock['commodity_id']
            }
        })
    } else if (stock['commodity_type'] == enums.CommodityType.Product) {
        commodity = await Product.findAll({
            attributes: ["name","preview_pic_url"],
            where: {
                "id": stock['commodity_id']
            }
        })
    } else {
        return res.json({
            ret_code: ec.RES_NOT_EXISTS.code,
            ret_msg: ec.RES_NOT_EXISTS.message,
        });
    }

    if (commodity.length < 1) {
        return res.json({
            ret_code: ec.RES_NOT_EXISTS.code,
            ret_msg: ec.RES_NOT_EXISTS.message,
        });
    }
    commodity = commodity[0]
*/

    let data = {}




    if(req.session['openId'] == "oi3CX5LuCW-jXQx6AzXk55NDVlXM"){
        totalfee = 1
    }

    data['id'] = dataHelper.generateOrderid()
   // data['preview_pic_url'] = commodity['preview_pic_url']
    data['stock_id'] ='null'
    data['commodity_name'] = 'null'
    data['total_fee'] = totalfee
    data['fee_type'] = stocks[0]['fee_type']
    data['num'] = 0
    data['cash_fee'] = totalfee
    data['cash_fee_type'] = stocks[0]['fee_type']
    data['client_ip'] = '127.0.0.1'
    data['time_start'] = Date.now()
    data['openid'] = req.session.openId
    data['remark'] = param['remark'] ? param['remark'] : ""
    data['contact_name'] = param['contact_name']
    data['contact_addr'] = param['contact_addr']
    data['contact_phone'] = param['contact_phone']
    data['create_time'] = Date.now()
    data['modify_time'] = Date.now()
    data['modifier'] = 'user'
    data['trade_state'] = 0
    data['pay_channel_type'] = 1

    let time_expire = moment().add(12, "hours").format("YYYYMMDDHHmmss")
    let wxOpts = {
        body: data['commodity_name'],
        out_trade_no: data['id'],
        fee_type: data['cash_fee_type'],
        total_fee: data['cash_fee'],
        spbill_create_ip: '127.0.0.1',
        notify_url: globalContext.config['wx']['pay']['nofityUrl'],
        trade_type: 'JSAPI',
        openid:req.session['openId'],
        time_expire
    }
    let order_stock_data=[];
    param.order.map((item)=>{
        order_stock_data.push({
            stock_id: item.stock_id,
            order_id:data['id'],
            spec_name: item.spec_name,
            name: item.productName,
            preview_pic_url: item.productImg,
            fee: item.fee,
            fee_type: data['fee_type'],
            num: item.num,
            create_time:Date.now(),
        })
    })

    let transaction = null;

    try {

        let result = await wx.Unifiedorder(wxOpts)
        logger.info(result)
        if (result['return_code'] != "SUCCESS" || result['result_code'] != "SUCCESS") {
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
        let prepay_id = result['prepay_id']

        transaction = await globalContext.database['mysql']['db_muller'].transaction();
        /*
               await CommodityStock.decrement('num', { where:whereRule,by: param['num'], transaction })

             //判断订单是否超卖
               let stock = await CommodityStock.findAll(queryParam, { transaction });
               stock = JSON.parse(JSON.stringify(stock));
               if (stock.length < 1) {
                   return res.json({
                       ret_code: ec.RES_NOT_EXISTS.code,
                       ret_msg: ec.RES_NOT_EXISTS.message,
                   });
               }
               stock = stock[0];

               //检查库存
               if (stock['num'] < 0) {
                   throw { msg: "the stock is not enougth" }
               }*/
        await Order.create(data, { transaction })
        await OrderStock.bulkCreate(order_stock_data,{ transaction })
        await transaction.commit()
        let resOpt = {
            package:`prepay_id=${prepay_id}`,
            appId:globalContext.config['wx']['login']['appId'],
            nonceStr:`${new Date().getTime()}${Math.ceil(Math.random()*100)}`,
            signType:'MD5',
            timeStamp:new Date().getTime()

        }
        cache.DelPersonal(req.session['userId'])
        return res.json({
            ret_code: ec.SUCCESS.code,
            ret_msg: ec.SUCCESS.message,
            ...wx.getAppSign(resOpt),
            order_id: data['id']
        });
    } catch (e) {
        if (transaction) {
            await transaction.rollback();
        }

        logger.printExecption(e);
        return res.json({
            ret_code: ec.FAILED.code,
            ret_msg: ec.FAILED.message,
        });

    }
}


async function getB2cOrderDetail(req, res) {

    let logger = req.logger
    let param = req.query;

    try {
        cu.checkFieldExists(param, 'id');
    } catch (e) {
        logger.printExecption(e);
        return res.json({
            ret_code: ec.PARAM_ERROR.code,
            ret_msg: e.message || ec.PARAM_ERROR.message,
        });
    }

    let whereRule = {};

    whereRule['$t_b2c_order.id$'] = {
        '$eq': param['id'],
    }
    whereRule['$t_b2c_order.lstate$'] = {
        '$eq': enums.DataLState.ACTIVE,
    }
    whereRule['$t_b2c_order.openid$'] = {
        '$eq': req.session['openId']
    }

    const B2COrder = cu.getModel('b2c_order');
    const B2COrderRoll = cu.getModel('b2c_order_roll');

    let queryParam = {
        attributes: [
            'id',
            'total_fee',
            'cash_fee',
            'num',
            'time_start',
            'time_pay',
            'time_end',
            'trade_state',
            'trade_state_desc',
            "contact_addr",
            'remark',
            'contact_name',
            'contact_phone',
            'contact_addr',
            'pay_channel_type',
            'modifier',
            'modify_time',
            'create_time',
        ],
    }

    queryParam['where'] = whereRule;

    try {
        let order = await B2COrder.findAll(queryParam);
        order = JSON.parse(JSON.stringify(order));

        if (order.length < 1) {
            return res.json({
                ret_code: ec.RES_NOT_EXISTS.code,
                ret_msg: ec.RES_NOT_EXISTS.message,
            });
        }

        let data = order[0];

        if (data['trade_state'] == enums.OrderTradeState.NotPay) {
            let opt = {
                out_trade_no: param['id']
            }
            let result = await wx.orderquery(opt)
            if (result['return_code'] == "SUCCESS" || result['result_code'] == "SUCCESS") {
                data['trade_state'] = enums.TRADE_STATE[result['trade_state']]
            }
            if(data['trade_state']!=enums.OrderTradeState.NotPay){
                //try to invoke a task
                UpdateWxOrder(param['id'],result)
            }
        }

        console.log(data);


        let spec = await dataHelper.GetOrderStock([param['id']])
        data['stocks'] = spec[param['id']]
        let roll = await B2COrderRoll.findAll({
            attributes: [
                'trade_state',
                'trade_state_desc',
                'modify_time',
                'modifier',
            ],
            where: {
                'b2c_order_id': param['id'],
            },
            order: [
                ['modify_time', 'DESC']
            ]
        });

        data['order_roll'] = roll;
        data['ret_code'] = ec.SUCCESS.code;
        data['ret_msg'] = ec.SUCCESS.message;
        return res.json(data);
    } catch (e) {
        logger.printExecption(e);
        return res.json({
            ret_code: ec.FAILED.code,
            ret_msg: ec.FAILED.message,
        });
    }


}



async function getB2cOrderList(req,res){
    let logger = req.logger
    let param = req.query;

    ci.handlePageParam(param);

    let orderRule = [
        ['time_start', 'DESC']
    ];
    let limitRule = param['size'];
    let whereRule = {
        '$t_b2c_order.lstate$': {
            '$eq': enums.DataLState.ACTIVE,
        },
        '$t_b2c_order.openid$': {
            '$eq': req.session['openId'],
        }
    };

    if (param['query_commodity_name']) {
        whereRule['$t_b2c_order.commodity_name$'] = {
            '$like': '%' + param['query_commodity_name'] + '%',
        }
    }

    if (param['query_contact_name']) {
        whereRule['$t_b2c_order.contact_name$'] = {
            '$like': '%' + param['query_contact_name'] + '%',
        }
    }

    if (param['b2c_order_id']) {
        whereRule['$t_b2c_order.id$'] = {
            '$eq': param['b2c_order_id'],
        }
    }

    if (param['trade_state']) {
        whereRule['$t_b2c_order.trade_state$'] = {
            '$eq': param['trade_state'],
        }
    }

    if (param['time_start_begin']) {
        whereRule['$t_b2c_order.time_start$'] = {
            '$gt': param['time_start_begin'],
        }
    }

    if (param['time_start_end']) {
        whereRule['$t_b2c_order.time_start$'] = {
            '$lt': param['time_start_end'],
        }
    }


    const B2COrder = cu.getModel('b2c_order');
    let queryParam = {
        attributes: [
            'id',
            'stock_id',
            'commodity_name',
            'preview_pic_url',
            'total_fee',
            'cash_fee',
            'num',
            'time_start',
            'time_pay',
            'time_end',
            'trade_state',
            'trade_state_desc',
            'remark',
            'contact_name',
            'contact_phone',
            'pay_channel_type',
            'modifier',
            'modify_time',
            'create_time',
        ],
    }

    queryParam['offset'] = (param.page - 1) * param.size;
    queryParam['order'] = orderRule;
    queryParam['limit'] = limitRule;
    queryParam['where'] = whereRule;

    try {
        let orders = await B2COrder.findAndCountAll(queryParam);
        let count = orders.count;
        orders = orders.rows;
        orders = JSON.parse(JSON.stringify(orders));


        let orderIds = []
        orders.map((obj) => {
            orderIds.push(obj['id'])
            return obj;
        });

        let spec = await dataHelper.GetOrderStock(orderIds)
        orders.map((obj) => {
            console.log('obj',obj)
            console.log('spec',spec)
            if(spec[obj.id]){
                obj['stocks'] = spec[obj['id']]
            }
            return obj;
        });

        return res.json({
            ret_code: ec.SUCCESS.code,
            ret_msg: ec.SUCCESS.message,
            list: orders,
            size: orders.length,
            page: param.page,
            total_num: count,
        });
    } catch (e) {
        logger.printExecption(e);
        return res.json({
            ret_code: ec.FAILED.code,
            ret_msg: ec.FAILED.message,
        });
    }
}



async function getShoppingcartDetail(req, res) {

    let logger = req.logger
    let param = req.body;
    const CommodityStock = cu.getModel('commodity_stock')
    try {
        cu.checkFieldExists(param, 'ids');
    } catch (e) {
        logger.printExecption(e);
        return res.json({
            ret_code: ec.PARAM_ERROR.code,
            ret_msg: e.message || ec.PARAM_ERROR.message,
        });
    }

    console.log(typeof param.ids)
    let whereRule = {};
    whereRule['$t_commodity_stock.id$'] = {
        '$in': param.ids,
    }

    whereRule['$t_commodity_stock.lstate$'] = {
        '$eq': enums.DataLState.ACTIVE,
    }

    let queryParam = {
        attributes: [
            'id',
            'fee',
            'fee_type',
            'num',
            'spec_name',
            'commodity_id',
            'commodity_type'
        ],
    }

    queryParam['where'] = whereRule;
    let stocks = await CommodityStock.findAll(queryParam);
    stocks = JSON.parse(JSON.stringify(stocks));
    return res.json({
        ret_code: ec.SUCCESS.code,
        ret_msg: ec.SUCCESS.message,
        stocks: stocks
    });
}
