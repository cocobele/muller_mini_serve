
const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH + 'include/global');
const request = require('request-promise-native');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');
const Core = require('@alicloud/pop-core');
const wx = require('./wx')
const dataHelper = require("./data")

module.exports = {
    wxPayMsg
}

/**
 * 接受微信订单状态回调
 * @param {object} req 请求
 * @param {object} res 响应
 */
async function wxPayMsg(req, res) {
    let logger = req.logger
    logger.info("wx call back")
    logger.info(JSON.stringify(req.body))
    let errMsg = `<xml>
    <return_code><![CDATA[INVALID_REQUEST]]></return_code>
    <return_msg><![CDATA[INVALID_REQUEST]]></return_msg>
  </xml>`

    let retryMsg = `<xml>
  <return_code><![CDATA[SYSTEMERROR]]></return_code>
  <return_msg><![CDATA[SYSTEMERROR]]></return_msg>
</xml>`

    let sucMsg = `<xml>
  <return_code><![CDATA[SUCCESS]]></return_code>
  <return_msg><![CDATA[OK]]></return_msg>
</xml>`

    let param = req.body['xml']
    if (!param) {
        return res.end(errMsg)
    }

    logger.log(JSON.stringify(param))

    try {
        cu.checkFieldExists(param, 'return_code');


        wx.RspCodeCheck(param)

        cu.checkFieldExists(param, 'appid');
        cu.checkFieldExists(param, 'mch_id');


        wx.accountCheck(param)

        cu.checkFieldExists(param, 'nonce_str');
        cu.checkFieldExists(param, 'sign');
        cu.checkFieldExists(param, 'result_code');
        cu.checkFieldExists(param, 'openid');
        cu.checkFieldExists(param, 'is_subscribe');
        cu.checkFieldExists(param, 'trade_type');
        cu.checkFieldExists(param, 'bank_type');
        cu.checkFieldExists(param, 'total_fee');
        cu.checkFieldExists(param, 'cash_fee');
        cu.checkFieldExists(param, 'transaction_id');
        cu.checkFieldExists(param, 'out_trade_no');
        cu.checkFieldExists(param, 'cash_fee');
        cu.checkFieldExists(param, 'time_end');

        wx.checkSign(param)

    } catch (err) {
        logger.printExecption(err);
        logger.printExecption(JSON.stringify(err));

        return res.end(errMsg)
    }
    //todo 查询订单是否存在

    try{
        console.log(param['out_trade_no'],param)
        await dataHelper.UpdateWxOrder(param['out_trade_no'],param)
        return res.end(sucMsg)
    }catch(e){
        logger.printExecption(e);
        return res.end(errMsg)
    }
}
