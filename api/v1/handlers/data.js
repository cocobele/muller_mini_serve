
const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH+'/include/global');
const request = require('request-promise-native');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');
const Core = require('@alicloud/pop-core');
const wx = require("./wx")


module.exports = {
    GetArtworkCollectedStatus,
    GetExhibitionCollectedStatus,
    GetProductCollectedStatus,
    generateOrderid,
    getArtistofArticle,
    UpdateWxOrder,
    GetPersonal,
    GetOrderSpec,
    GetOrderStock
}




/****exhibition */
async function GetExhibitionByUserId(userId) {





}


async function GetExhibitionByArtist(artistId) {

}

/****artwork */
async function GetArtworkByArtist(artistId) {

}

async function GetArtworkByUserId(userId) {

}

async function GetArtworkByExhibition(exhibitonId) {

}

/****article */
async function GetExhibitionByUserId(userId) {

}


async function GetExhibitionByArtist(artistId) {

}


function generateOrderid() {
    return uuidv1().replace(/-/g, '');
}



async function GetArtworkCollectedStatus(userId, ids) {
    const MergeMiniUserArtwork = cu.getModel('merge_mini_user_artwork');
    let CollectedIds = {}
    for (let i = 0; i < ids.length; i++) {
        CollectedIds[ids[i]] = 0
    }
    await MergeMiniUserArtwork.findAll({
        attributes: ["lstate", "artwork_id"],
        where: {
            "mini_user_id": userId,
            "lstate": enums.DataLState.ACTIVE,
            "artwork_id": ids
        }
    }).map((obj) => {
        CollectedIds[obj['artwork_id']] = 1
    })
    return CollectedIds
}

async function GetProductCollectedStatus(userId, ids) {
    const MergeMiniUserProduct = cu.getModel('merge_mini_user_product');
    let CollectedIds = {}
    for (let i = 0; i < ids.length; i++) {
        CollectedIds[ids[i]] = 0
    }
    await MergeMiniUserProduct.findAll({
        attributes: ["lstate", "product_id"],
        where: {
            "mini_user_id": userId,
            "lstate": enums.DataLState.ACTIVE,
            "product_id": ids
        }
    }).map((obj) => {
        console.log("the merge is :", obj)
        CollectedIds[obj['product_id']] = 1
    })
    return CollectedIds
}


async function GetExhibitionCollectedStatus(userId, ids) {
    const MergeMiniUserExhibition = cu.getModel('merge_mini_user_exhibition');
    let CollectedIds = {}
    for (let i = 0; i < ids.length; i++) {
        CollectedIds[ids[i]] = 0
    }
    await MergeMiniUserExhibition.findAll({
        attributes: ["lstate", "exhibition_id"],
        where: {
            "mini_user_id": userId,
            "lstate": enums.DataLState.ACTIVE,
            "exhibition_id": ids
        }
    }).map((obj) => {

        CollectedIds[obj['exhibition_id']] = 1
    })
    return CollectedIds
}


async function getArtistofArticle(ids) {
    const MergeArtistArticle = cu.getModel('merge_artist_article');
    const Artist = cu.getModel('artist');
    MergeArtistArticle.belongsTo(Artist, { foreignKey: 'artist_id', targetKey: 'id' })
    let include = {
        model: Artist,
        attributes: ["id", "name"],
        where: {
            "lstate": enums.DataLState.ACTIVE,
        }
    }
    artists = await MergeArtistArticle.findAll({
        attributes: ["article_id"],
        include,
        where: {
            "article_id": ids,
            "lstate": enums.DataLState.ACTIVE
        }
    })
    artists = JSON.parse(JSON.stringify(artists))

    let data = {}
    for (let a of artists) {

        if (!data[a['article_id']]) {
            data[a['article_id']] = []
        }
        data[a['article_id']].push(a['t_artist'])

    }
    return data

}

/**
 *
 * @param {订单ID} id
 * @param {微信返回结果}} param
 */
async function UpdateWxOrder(id, param) {
    let data = {}
    data['id'] = param['out_trade_no']
    data['total_fee'] = param['total_fee']?param['total_fee']:0
    data['fee_type'] = param['fee_type']
    data['cash_fee'] = param['cash_fee']
    data['cash_fee_type'] = param['cash_fee_type'] ? param['cash_fee_type'] : "CNY"
    data['openid'] = param['openid']
    data['end_time'] = param['time_end']
    data['create_time'] = Date.now()
    data['modify_time'] = Date.now()
    data['modifier'] = 'admin'
    data['num'] = 0
    data['pay_channel_type'] = 1

    if(!data['trade_state']){
        if (param['result_code'] == "SUCCESS") {
            data['trade_state'] = 11
        } else {
            data['trade_state'] = 21
        }
    }


    const B2COrder = cu.getModel('b2c_order');
    await B2COrder.upsert(data, {
        fields: ["total_fee", "fee_type", "cash_fee", "cash_fee_type", "open_id", "end_time", "modify_time", "trade_state", "pay_channel_type", "modifier"]
    })

}


async function SetOrderInvalidOrder(data){

    const B2COrder = cu.getModel('b2c_order');
    const CommodityStock = cu.getModel('commodity_stock')


    //处理库存

    let transaction = null;
    try{
        transaction = await globalContext.database['mysql']['db_muller'].transaction();
        //恢复库存
        if(data['num'] > 0){
            await CommodityStock.increment('num', { where:{
                "id":{
                    "$eq":data['stock_id']
                }
            },by: data['num'], transaction })
        }
        await B2COrder.update({
            trade_state:data['trade_state'],
            trade_state_desc:data['trade_state_desc']
        }, {
            where: {
                id: data['id'],
            },
            transaction,
        });

        await transaction.commit();
    }catch(e){

        logger.printExecption(e)

        if(transaction){
            await transaction.rollback();
        }

    }
}

updateOrderLoop()
async function updateOrderLoop() {

    const B2COrder = cu.getModel('b2c_order');
    let queryParam = {
        attributes: [
            'id',
            'num',
            'stock_id',
            'trade_state',
            'create_time'
        ],
    }

    let whereRule = {};

    whereRule['$t_b2c_order.lstate$'] = {
        '$eq': enums.DataLState.ACTIVE,
    }
    whereRule['$t_b2c_order.pay_channel_type$'] = {
        '$eq': enums.PayChannelType.WxPay
    }
    whereRule['$t_b2c_order.trade_state$'] = {
        '$eq': enums.OrderTradeState.NotPay,
    }

    queryParam['limit'] = 20;
    while (true) {
        whereRule['$t_b2c_order.create_time$'] = {
            '$lt': new Date(new Date().getTime() - 5)
        }
        queryParam['where'] = whereRule;

        try {
            let order = await B2COrder.findAll(queryParam);
            order = JSON.parse(JSON.stringify(order));
            for(let data of order){
                if (data['trade_state'] == enums.OrderTradeState.NotPay) {
                    if(Date.now() - moment(data['create_time']).valueOf() >12 * 60 * 60 * 1000){
                        data['trade_state_desc'] = '订单过期'
                        data['trade_state'] = enums.OrderTradeState.Expire
                        await SetOrderInvalidOrder(data)
                        continue
                    }

                    let opt = {
                        out_trade_no: data['id']
                    }
                    let result = await wx.orderquery(opt)
                    logger.info(result)
                    if (result['return_code'] != "SUCCESS"){
                        continue
                    }

                    if(result['result_code'] == "SUCCESS") {
                        data['trade_state'] = enums.TRADE_STATE[result['trade_state']]
                    } else {

                        if(result['err_code'] =="ORDERNOTEXIST"){
                            data['trade_state_desc'] = '订单不存在'
                            data['trade_state'] = enums.OrderTradeState.NotExist
                            await SetOrderInvalidOrder(data)
                            continue
                        }
                    }
                    if (data['trade_state']) {
                        result['out_trade_no'] = data['id']
                        //try to invoke a task
                        await UpdateWxOrder(data['id'], result)
                    }
                }
            }

        } catch (err) {
            logger.printExecption(err);
        }finally{
            await cu.delay(10000)
        }
    }
}


async function GetPersonal(userId,openId){

    const MiniUser = cu.getModel('mini_user');
    const MergeMiniUserProduct = cu.getModel('merge_mini_user_product')
    const MergeMiniUserArtwork = cu.getModel('merge_mini_user_artwork')
    const MergeMiniUserExhibition= cu.getModel('merge_mini_user_exhibition')
    const B2COrder = cu.getModel('b2c_order');
    const Artwork = cu.getModel("artwork")
    const Exhibition = cu.getModel('exhibition')
    const Product = cu.getModel('product')

    let data = {}

    let info = await MiniUser.findOne({
        attributes:["name","email","address","phone","logo"],
        where:{
            "id":userId
        }
    })

    data['name'] = info.name
    data['email'] = info.email
    data['address'] = info.address
    data['phone'] = info.phone
    data['logo'] = info.logo
    data['collect_count'] = 0

    let whereRule = {
        "lstate":enums.DataLState.ACTIVE
    }

    MergeMiniUserArtwork.belongsTo(Artwork, { foreignKey: 'artwork_id', targetKey: 'id' })
    let include = {
        attributes:['id'],
        model: Artwork,
        where: whereRule
    }
    let count = await MergeMiniUserArtwork.count({
        attributes: [],
        include,
        where: {
            "mini_user_id": userId,
            "lstate": enums.DataLState.ACTIVE,
            ...whereRule
        }
    })
    data['collect_count'] = data['collect_count'] + count



    MergeMiniUserExhibition.belongsTo(Exhibition, { foreignKey: 'exhibition_id', targetKey: 'id' })
     include = {
        attributes:['id'],
        model: Exhibition,
        where: whereRule
    }
     count = await MergeMiniUserExhibition.count({
        attributes: [],
        include,
        where: {
            "mini_user_id": userId,
            "lstate": enums.DataLState.ACTIVE,
            ...whereRule
        }
    })
    data['collect_count'] = data['collect_count'] + count


    MergeMiniUserProduct.belongsTo(Product, { foreignKey: 'product_id', targetKey: 'id' })
    include = {
       attributes:['id'],
       model: Product,
       where: whereRule
   }
    count = await MergeMiniUserProduct.count({
       attributes: [],
       include,
       where: {
           "mini_user_id": userId,
           "lstate": enums.DataLState.ACTIVE,
           ...whereRule
       }
   })
   data['collect_count'] = data['collect_count'] + count


    data['order_count'] = await B2COrder.count({
        where:{
            "openid": openId,
            "lstate": enums.DataLState.ACTIVE,
        }
    })
    return data
}


/**
 * 获取指定订单ID的规格，图片等信息
 * @param {*} ids stock ID
 */
async function GetOrderSpec(ids){
    const CommodityStock = cu.getModel('commodity_stock')
    let queryParam = {
        attributes:["id","spec_name"],
        where:{
            "id":ids
        }
    }
    let stock = await CommodityStock.findAll(queryParam);
    stock = JSON.parse(JSON.stringify(stock));
    let result = {}
    stock.forEach(element => {
        result[element['id']] = element
    });
    return result
}

/**
 * 获取指定订单ID的规格，图片等信息
 * @param {*} ids stock ID
 */
async function GetOrderStock(ids){
    const orderStock = cu.getModel('order_stock')
    let queryParam = {
        attributes:["order_id","stock_id","name","spec_name","preview_pic_url","fee","num"],
        where:{
            "order_id":ids
        }
    }
    let stock = await orderStock.findAll(queryParam);
    stock = JSON.parse(JSON.stringify(stock));
    console.log('stock',stock)
    let result = {}
    stock.forEach(element => {
        result[element['order_id']]=result[element['order_id']]||[]
        result[element['order_id']].push(element)
    });
    console.log('GetOrderStock',result)
    return result
}
