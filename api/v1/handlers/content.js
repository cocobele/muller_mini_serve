const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const request = require('request-promise-native');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');
const Core = require('@alicloud/pop-core');
const dataHelper = require("./data")


module.exports = {

    getArtistDetail: async function (req, res) {
        let logger = req.logger
        let param = req.query;
        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};
        whereRule['$t_artist.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_artist.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const Artist = cu.getModel('artist');
        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'pic_url',
                'introduce',
            ],
        }
        queryParam['where'] = whereRule;

        try {
            let artist = await Artist.findAll(queryParam);
            artist = JSON.parse(JSON.stringify(artist));
            if (artist.length < 1) {
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }
            let data = artist[0];
            data['pic_url'] = data['pic_url'].split('|');
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }


    },

    getArtistList: async function (req, res) {

        let logger = req.logger
        let param = req.query;
        ci.handlePageParam(param);
        let orderRule = [
            ['weight', 'DESC'],
            ['modify_time', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_artist.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if (param['query_name']) {
            whereRule['$t_artist.name$'] = {
                '$like': '%' + param['query_name'] + '%',
            }
        }


            whereRule['$t_artist.disp_state$'] = {
                '$eq': 1
            }


        let tsNow = moment().unix();

        const Artist = cu.getModel('artist');
        const MergeArtistArtwork = cu.getModel('merge_artist_artwork');
        const MergeArtistArticle = cu.getModel('merge_artist_article');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'pic_url',
                'introduce',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let artists = await Artist.findAndCountAll(queryParam);
            let count = artists.count;
            artists = artists.rows;
            artists = JSON.parse(JSON.stringify(artists));

            let artistIds = artists.map((obj) => {
                return obj['id'];
            });
            let countMap = {};
            let countArtwork = await MergeArtistArtwork.count({
                where: {
                    'artist_id': artistIds,
                },
                group: 'artist_id',
            });
            let countArticle = await MergeArtistArticle.count({
                where: {
                    'artist_id': artistIds,
                },
                group: 'artist_id',
            });
            for (let i = 0; i < artistIds.length; i++) {
                countMap[artistIds[i]] = {
                    'article': countArticle[i] && countArticle[i]['count'] ? countArticle[i]['count'] : 0,
                    'artwork': countArtwork[i] && countArtwork[i]['count'] ? countArtwork[i]['count'] : 0,
                }
            }
            // logger.debug(countMap);

            artists.map((obj) => {
                obj['pic_url'] = obj['pic_url'].split('|');
                obj['artwork_num'] = countMap[obj['id']]['artwork'];
                obj['article_num'] = countMap[obj['id']]['article'];

                return obj;
            });

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: artists,
                size: artists.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }

    },

    getArticleList: async function (req, res) {

        let logger = req.logger
        let param = req.query;

        ci.handlePageParam(param);

        let orderRule = [
            ['weight', 'DESC'],
            ['modify_time', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_article.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        whereRule['$t_article.disp_state$'] = {
            '$eq': 1
        }

        if (param['query_title']) {
            whereRule['$t_article.title$'] = {
                '$like': '%' + param['query_title'] + '%',
            }
        }

        const Article = cu.getModel('article');
        const MergeArtistArticle = cu.getModel('merge_artist_article');
        let queryParam = {
            attributes: [
                'id',
                'title',
                'disp_state',
                'preview_pic_url',
                'abstract',
                'content',
                'weight',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let count = 0
            let articles = []
            if (param['artist_id']) {
                //查询艺术家
                MergeArtistArticle.belongsTo(Article, { foreignKey: 'article_id', targetKey: 'id' })
                let include = {
                    model: Article,
                    attributes: queryParam.attributes,
                    where: queryParam.whereRule,
                    order: orderRule
                }
                articles = await MergeArtistArticle.findAndCountAll({
                    attributes: [],
                    include,
                    where: {
                        "artist_id": param['artist_id'],
                        "lstate": enums.DataLState.ACTIVE,
                        ...whereRule
                    },
                    offset: (param.page - 1) * param.size,
                    limit: limitRule

                })
                count = articles.length
                articles = articles.rows;
                articles = JSON.parse(JSON.stringify(articles));
                for (let i = 0; i < articles.length; i++) {
                    articles[i] = articles[i]['t_article']
                }
            } else {
                articles = await Article.findAndCountAll(queryParam);
                count = articles.count;
                articles = articles.rows;
                articles = JSON.parse(JSON.stringify(articles));
            }

            let articleIDs = []
            articles.map((obj) => {
                articleIDs.push(obj['id'])
                return obj;
            });

            let artist = await dataHelper.getArtistofArticle(articleIDs)

            articles.map((obj) => {
                obj["artist"] = artist[obj['id']]
                if(obj['artist']){
                    obj['writer'] = obj['artist'][0]['name']
                }
                return obj;
            });


            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: articles,
                size: articles.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }


    },
    getArticleDetail: async function (req, res) {
        let logger = req.logger
        let param = req.query;

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};

        whereRule['$t_article.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_article.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const Article = cu.getModel('article');

        let queryParam = {
            attributes: [
                'id',
                'title',
                'preview_pic_url',
                'abstract',
                'content',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let article = await Article.findAll(queryParam);
            article = JSON.parse(JSON.stringify(article));

            if (article.length < 1) {
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            let data = article[0];

            let artist = await dataHelper.getArtistofArticle([data['id']])

            if(artist[data['id']]){
                data["artist"] = artist[data['id']]
                data['writer'] = data['artist'][0]['name']
            }
            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }

    },
    getProductCateList: async function (req, res) {
        let logger = req.logger
        let param = req.query;

        ci.handlePageParam(param);

        let orderRule = [
            ['modify_time', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_product_cate.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if (param['query_name']) {
            whereRule['$t_product_cate.name$'] = {
                '$like': '%' + param['query_name'] + '%',
            }
        }

        whereRule['$t_product_cate.disp_state$'] = {
            '$eq': 1
        }

        let tsNow = moment().unix();

        const ProductCate = cu.getModel('product_cate');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
                'preview_pic_url'
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let productCate = await ProductCate.findAndCountAll(queryParam);
            let count = productCate.count;
            productCate = productCate.rows;
            productCate = JSON.parse(JSON.stringify(productCate));

            productCate.map((obj) => {
                console.log("@@@@@@@@@@@@@@@@@@",obj)
                if(!obj.preview_pic_url){
                    obj.preview_pic_url = "http://yanshan-upload.oss-cn-shenzhen.aliyuncs.com/pic/2604/c7a0d33abc2bd2fb3728da9d56e21c2758fe7f0f292ece103239a321c84c2dac"
                }
                return obj
            })

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: productCate,
                size: productCate.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }



    },
    getBrandList: async function (req, res) {

        let logger = req.logger
        let param = req.query;

        ci.handlePageParam(param);

        let orderRule = [
            ['weight', 'DESC'],
            ['modify_time', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_brand.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if (param['query_name']) {
            whereRule['$t_brand.name$'] = {
                '$like': '%' + param['query_name'] + '%',
            }
        }

        whereRule['$t_brand.disp_state$'] = {
            '$eq': 1
        }

        let tsNow = moment().unix();

        const Brand = cu.getModel('brand');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'pic_url',
                'introduce',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let brands = await Brand.findAndCountAll(queryParam);
            let count = brands.count;
            brands = brands.rows;
            brands = JSON.parse(JSON.stringify(brands));
            brands.map((obj) => {

                if(obj.pic_url){
                    obj.pic_url = obj.pic_url.split("|")
                } else {
                    obj.pic_url = []
                }

                return obj
            })

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: brands,
                size: brands.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }

    },
    getBrandDetail: async function (req, res) {

        let logger = req.logger
        let param = req.query;

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};

        whereRule['$t_brand.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_brand.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const Brand = cu.getModel('brand');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'pic_url',
                'introduce',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let brand = await Brand.findAll(queryParam);
            brand = JSON.parse(JSON.stringify(brand));

            if (brand.length < 1) {
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            let data = brand[0];
            data['pic_url'] = data['pic_url'].split('|');

            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }

    },
    getExhibitionList: async function (req, res) {

        let logger = req.logger
        let param = req.query;

        ci.handlePageParam(param);

        let orderRule = [
            ['weight', 'DESC'],
            ['modify_time', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_exhibition.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if (param['query_name']) {
            whereRule['$t_exhibition.name$'] = {
                '$like': '%' + param['query_name'] + '%',
            }
        }


        whereRule['$t_exhibition.disp_state$'] = {
            '$eq': 1
        }

        if (param['open_time']) {
            whereRule['$t_exhibition.open_time$'] = {
                '$gt': param['open_time'],
            }
        }

        if (param['close_time']) {
            whereRule['$t_exhibition.close_time$'] = {
                '$lt': param['close_time'],
            }
        }

        let tsNow = moment().unix();

        const Exhibition = cu.getModel('exhibition');
        const MergeExhibitionArtwork = cu.getModel('merge_exhibition_artwork');
        const MergeExhibitionArtist = cu.getModel('merge_exhibition_artist');
        const MergeMiniUserExhibition = cu.getModel('merge_mini_user_exhibition')

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'preview_pic_url',
                'pic_url',
                'introduce',
                'open_time',
                'close_time',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {

            let exhibitions = []
            let count = 0
            if (param['user_id']) {
                //查询个人收藏

                MergeMiniUserExhibition.belongsTo(Exhibition, { foreignKey: 'exhibition_id', targetKey: 'id' })
                let include = {
                    model: Exhibition,
                    attributes: queryParam.attributes,
                    where: queryParam.whereRule,
                    order: orderRule
                }
                exhibitions = await MergeMiniUserExhibition.findAndCountAll({
                    attributes: [],
                    include,
                    where: {
                        "mini_user_id": param['user_id'],
                        "lstate": enums.DataLState.ACTIVE,
                        ...whereRule
                    },
                    offset: (param.page - 1) * param.size,
                    limit: limitRule

                })
                count = exhibitions.count;
                exhibitions = exhibitions.rows;
                exhibitions = JSON.parse(JSON.stringify(exhibitions));
                for (let i = 0; i < exhibitions.length; i++) {
                    exhibitions[i] = exhibitions[i]['t_exhibition']
                }

            } else if (param['artist_id']) {
                //查询艺术家
                MergeExhibitionArtist.belongsTo(Exhibition, { foreignKey: 'exhibition_id', targetKey: 'id' })
                let include = {
                    model: Exhibition,
                    attributes: queryParam.attributes,
                    where: queryParam.whereRule,
                    order: orderRule
                }
                exhibitions = await MergeExhibitionArtist.findAndCountAll({
                    attributes: [],
                    include,
                    where: {
                        "artist_id": param['artist_id'],
                        "lstate": enums.DataLState.ACTIVE,
                        ...whereRule
                    },
                    offset: (param.page - 1) * param.size,
                    limit: limitRule

                })
                count = exhibitions.count;
                exhibitions = exhibitions.rows;
                exhibitions = JSON.parse(JSON.stringify(exhibitions));
                for (let i = 0; i < exhibitions.length; i++) {
                    exhibitions[i] = exhibitions[i]['t_exhibition']
                }

            } else {
                exhibitions = await Exhibition.findAndCountAll(queryParam);
                count = exhibitions.count;
                exhibitions = exhibitions.rows;
                exhibitions = JSON.parse(JSON.stringify(exhibitions));

            }

            let exhibitionIds = exhibitions.map((obj) => {
                return obj['id'];
            });
            let countMap = {};
            let countArtwork = await MergeExhibitionArtwork.count({
                where: {
                    'exhibition_id': exhibitionIds,
                },
                group: 'exhibition_id',
            });

            for (let i = 0; i < exhibitionIds.length; i++) {
                if (countArtwork[i]) {
                    countMap[exhibitionIds[i]] = {
                        'artwork': countArtwork[i]['count'],
                    }
                } else {
                    countMap[exhibitionIds[i]] = 0
                }
            }


            let CollectedIds = await dataHelper.GetExhibitionCollectedStatus(req.session['userId'],exhibitionIds)

            exhibitions.map((obj) => {
                obj['pic_url'] = obj['pic_url'].split('|');
                obj['artwork_num'] = countMap[obj['id']]['artwork'];
                if(!obj['preview_pic_url'] && obj['pic_url'].length > 0 ){
                    obj['preview_pic_url'] = obj['pic_url'][0]
                }

                if (CollectedIds[obj['id']]) {
                    obj['collected_status'] = 1

                } else {
                    obj['collected_status'] = 0
                }

                return obj;
            });

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: exhibitions,
                size: exhibitions.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }

    },
    getExhibitionDetail: async function (req, res) {
        let logger = req.logger
        let param = req.query;

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};

        whereRule['$t_exhibition.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_exhibition.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }


        const Exhibition = cu.getModel('exhibition');
        const MergeExhibitionArtwork = cu.getModel('merge_exhibition_artwork');
        const MergeExhibitionArtist = cu.getModel('merge_exhibition_artist');
        const Artwork = cu.getModel('artwork');
        const Artist = cu.getModel('artist');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'preview_pic_url',
                'pic_url',
                'introduce',
                'open_time',
                'close_time',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let exhibition = await Exhibition.findAll(queryParam);
            exhibition = JSON.parse(JSON.stringify(exhibition));

            if (exhibition.length < 1) {
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            let data = exhibition[0];
            data['pic_url'] = data['pic_url'].split('|');

            if(!data['preview_pic_url'] && data['pic_url'].length > 0 ){
                data['preview_pic_url'] = data['pic_url'][0]
            }

            let relArtworks = await MergeExhibitionArtwork.findAll({
                where: {
                    exhibition_id: param['id'],
                },
            });
            let artworkIds = relArtworks.map((obj) => {
                return obj['artwork_id'];
            });
            let artworks = await Artwork.findAll({
                where: {
                    lstate: {
                        '$eq': enums.DataLState.ACTIVE,
                    },
                    id: artworkIds,
                },
            });
            data['artwork'] = artworks;

            let relArtists = await MergeExhibitionArtist.findAll({
                where: {
                    exhibition_id: param['id'],
                },
            });
            let artistIds = relArtists.map((obj) => {
                return obj['artist_id'];
            });
            let artists = await Artist.findAll({
                where: {
                    lstate: {
                        '$eq': enums.DataLState.ACTIVE,
                    },
                    id: artistIds,
                },
            });
            data['artist'] = artists;


            let CollectedIds = await dataHelper.GetExhibitionCollectedStatus(req.session['userId'],[data['id']])
            data['collected_status'] = CollectedIds[data['id']]


            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },
    getArtworkList: async function (req, res) {

        let logger = req.logger
        let param = req.query;
        ci.handlePageParam(param);

        const Artwork = cu.getModel('artwork');
        const Artist = cu.getModel('artist');
        const MergeArtistArtwork = cu.getModel('merge_artist_artwork');
        const MergeExhibitionArtwork = cu.getModel('merge_exhibition_artwork');
        const MergeMiniUserArtwork = cu.getModel('merge_mini_user_artwork');
        const CommodityStock = cu.getModel('commodity_stock')

        let orderRule = [
            ['weight', 'DESC'],
            ['modify_time', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_artwork.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };
        whereRule['$t_artwork.disp_state$'] = {
            '$eq': 1
        }

        if (param['query_name']) {
            whereRule['$t_artwork.name$'] = {
                '$like': '%' + param['query_name'] + '%',
            }
        }

        if (param['disp_state']) {
            whereRule['$t_artwork.disp_state$'] = {
                '$eq': param['disp_state'],
            }
        }

        let artistIds = [];
        if (param['query_artist_name']) {
            artistIds = await Artist.findAll({
                attributes: [
                    'id',
                ],
                where: {
                    '$t_artist.name$': {
                        '$like': '%' + param['query_artist_name'] + '%',
                    }
                },
            }).map((obj) => {
                return obj['id'];
            });
        }



        // logger.debug(artistIds);
        // logger.debug(relArtistArtwork);

        let tsNow = moment().unix();

        let queryParam = {
            attributes: [
                'id',
                'name',
                'commodity_sale_type',
                'artist_name',
                'disp_state',
                'preview_pic_url',
                'pic_url',
                'introduce',
                'description',
                'audio_guide_id',
                'weight',
                'spec_info',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;



        let artworks = []
        let count = 0
        try {
            if (param['user_id']) {
                //查询个人收藏

                MergeMiniUserArtwork.belongsTo(Artwork, { foreignKey: 'artwork_id', targetKey: 'id' })
                let include = {
                    model: Artwork,
                    attributes: queryParam.attributes,
                    where: queryParam.whereRule,
                    order: orderRule
                }
                artworks = await MergeMiniUserArtwork.findAndCountAll({
                    attributes: [],
                    include,
                    where: {
                        "mini_user_id": param['user_id'],
                        "lstate": enums.DataLState.ACTIVE,
                        ...whereRule
                    },
                    offset: (param.page - 1) * param.size,
                    limit: limitRule

                })
                count = artworks.length
                artworks = artworks.rows;
                artworks = JSON.parse(JSON.stringify(artworks));
                for (let i = 0; i < artworks.length; i++) {
                    artworks[i] = artworks[i]['t_artwork']
                    artworks[i]['collected_status'] = enums.CollectedType.Collected
                }

            } else if (param['artist_id']) {
                //查询艺术家
                MergeArtistArtwork.belongsTo(Artwork, { foreignKey: 'artwork_id', targetKey: 'id' })
                let include = {
                    model: Artwork,
                    attributes: queryParam.attributes,
                    where: queryParam.whereRule,
                    order: orderRule
                }
                artworks = await MergeArtistArtwork.findAndCountAll({
                    attributes: [],
                    include,
                    where: {
                        "artist_id": param['artist_id'],
                        "lstate": enums.DataLState.ACTIVE,
                        ...whereRule
                    },
                    offset: (param.page - 1) * param.size,
                    limit: limitRule

                })
                count = artworks.length
                artworks = artworks.rows;
                artworks = JSON.parse(JSON.stringify(artworks));
                for (let i = 0; i < artworks.length; i++) {
                    artworks[i] = artworks[i]['t_artwork']
                }
            } else if (param['exhibition_id']) {

                //查询艺术家
                MergeExhibitionArtwork.belongsTo(Artwork, { foreignKey: 'artwork_id', targetKey: 'id' })
                let include = {
                    model: Artwork,
                    attributes: queryParam.attributes,
                    where: queryParam.whereRule,
                    order: orderRule
                }
                artworks = await MergeExhibitionArtwork.findAndCountAll({
                    attributes: [],
                    include,
                    where: {
                        "exhibition_id": param['exhibition_id'],
                        "lstate": enums.DataLState.ACTIVE,
                        ...whereRule
                    },
                    offset: (param.page - 1) * param.size,
                    limit: limitRule

                })
                count = artworks.length
                artworks = artworks.rows;
                artworks = JSON.parse(JSON.stringify(artworks));
                for (let i = 0; i < artworks.length; i++) {
                    artworks[i] = artworks[i]['t_artwork']
                }
            } else {
                artworks = await Artwork.findAndCountAll(queryParam);
                count = artworks.count;
                artworks = artworks.rows;
                artworks = JSON.parse(JSON.stringify(artworks));
            }

            artworks.map((obj) => {
                obj['pic_url'] = obj['pic_url'].split('|');
                return obj;
            });



            let saleIds = []
            let artworkIds = artworks.map((obj) => {
                if (obj['commodity_sale_type'] == 1) {
                    saleIds.push(obj['id'])
                }
                return obj['id'];
            });

            let relArtworkStock = {}
            await CommodityStock.findAll({
                attributes: [
                    'id','commodity_id', 'spec_name', 'fee', 'fee_type', 'num'
                ],
                where: {
                    'lstate': enums.DataLState.ACTIVE,
                    'commodity_id': saleIds,
                    'commodity_type': enums.CommodityType.ArtWork
                }
            }).map((obj) => {
                if (!relArtworkStock[obj.commodity_id]) {
                    relArtworkStock[obj.commodity_id] = []
                }
                relArtworkStock[obj.commodity_id].push(obj)
            })


            let CollectedIds = await dataHelper.GetArtworkCollectedStatus(req.session['userId'],artworkIds)

            // 设置作品艺术家
            let artistIds = [],
                relArtworkArtist = {};
            await MergeArtistArtwork.findAll({
                attributes: [
                    'artist_id',
                    'artwork_id',
                ],
                where: {
                    'artwork_id': artworkIds,
                },
            }).map((obj) => {
                artistIds.push(obj['artist_id']);
                if (!relArtworkArtist[obj['artwork_id']]) {
                    relArtworkArtist[obj['artwork_id']] = [];
                }
                if (relArtworkArtist[obj['artwork_id']]) {
                    relArtworkArtist[obj['artwork_id']].push(obj['artist_id']);
                }

            });
            let artistMap = {};
            await Artist.findAll({
                attributes: [
                    'id',
                    'name',
                    'pic_url',
                ],
                where: {
                    'id': artistIds,
                },
            }).map((obj) => {
                obj['pic_url'] = obj['pic_url'].split('|');
                artistMap[obj['id']] = obj;
            });


            //fill some data
            artworks.map((obj) => {
                obj['artist_list'] = [];
                if (relArtworkArtist[obj['id']]) {
                    relArtworkArtist[obj['id']].map((id) => {
                        obj['artist_list'].push(artistMap[id]);
                    });
                }

                obj['commodity_sale_list'] = []
                if (relArtworkStock[obj['id']]) {
                    obj['commodity_sale_list'] = relArtworkStock[obj['id']]
                }

                if (CollectedIds[obj['id']]) {
                    obj['collected_status'] = 1
                } else {
                    obj['collected_status'] = 0
                }

                if(obj['spec_info']){
                    try{
                        obj['spec_info'] = JSON.parse(obj['spec_info'])

                    }catch(err){
                        obj['spec_info'] = []
                    }
                }
            });

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: artworks,
                size: artworks.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }

    },
    getArtworkDetail: async function (req, res) {

        let logger = req.logger
        let param = req.query;
        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};

        whereRule['$t_artwork.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_artwork.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const Artwork = cu.getModel('artwork');
        const MergeArtistArtwork = cu.getModel('merge_artist_artwork');
        const CommodityStock = cu.getModel('commodity_stock');
        const Audio = cu.getModel('audio');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'artist_name',
                'disp_state',
                'preview_pic_url',
                'commodity_sale_type',
                'pic_url',
                'introduce',
                'description',
                'audio_guide_id',
                'modifier',
                'spec_info',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let artwork = await Artwork.findAll(queryParam);
            artwork = JSON.parse(JSON.stringify(artwork));

            if (artwork.length < 1) {
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            let data = artwork[0];
            data['pic_url'] = data['pic_url'].split('|');

            let relsArtwork = await MergeArtistArtwork.findAll({
                attributes: [
                    'artist_id',
                ],
                where: {
                    'artwork_id': param['id'],
                },
            });
            data['artist_id'] = relsArtwork.map((obj) => {
                return obj['artist_id'];
            });


            data['commodity_sale_list'] = []
            if (data['commodity_sale_type'] == 1) {
                await CommodityStock.findAll({
                    attributes: [
                        'id','commodity_id', 'spec_name', 'fee', 'fee_type', 'num'
                    ],
                    where: {
                        'lstate': enums.DataLState.ACTIVE,
                        'commodity_id': [data.id],
                        'commodity_type': enums.CommodityType.ArtWork
                    }
                }).map((obj) => {
                    data['commodity_sale_list'].push(obj)
                })
            }

            if (data['audio_guide_id']) {

                let audio = await Audio.findAll({
                    attributes: [ "mime", "audio_length", "file_size", "raw_url", "file_url"],
                    where: {
                        "lstate": enums.DataLState.ACTIVE,
                        "id": data['audio_guide_id']
                    }
                })

                audio = JSON.parse(JSON.stringify(audio));

                if (audio.length > 0) {
                    data['audio_guide'] = audio[0];
                }
            }

            if(data['spec_info']){
                try{
                    data['spec_info'] = JSON.parse(data['spec_info'])

                }catch(err){
                    data['spec_info'] = []
                }
            }

            let CollectedIds = await dataHelper.GetArtworkCollectedStatus(req.session['userId'],[data['id']])
            data['collected_status'] = CollectedIds[data['id']]
            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },






    getProductList: async function (req, res) {

        let logger = req.logger
        let param = req.query;
        ci.handlePageParam(param);
        let orderRule = [
            ['disp_state', 'ASC'],
            ['weight', 'DESC']
        ];
        let limitRule = param['size'];
        let whereRule = {
            '$t_product.lstate$': {
                '$eq': enums.DataLState.ACTIVE,
            }
        };

        if (param['query_name']) {
            whereRule['$t_product.name$'] = {
                '$like': '%' + param['query_name'] + '%',
            }
        }

        whereRule['$t_product.disp_state$'] = {
            '$eq': 1
        }

        if (param['brand_id']) {
            whereRule['$t_product.brand_id$'] = {
                '$like': '%' + param['brand_id'] + '%',
            }
        }

        if (param['product_cate_id']) {
            whereRule['$t_product.product_cate_id$'] = {
                '$like': '%' + param['product_cate_id'] + '%',
            }
        }

        let tsNow = moment().unix();

        const Product = cu.getModel('product');
        const Brand = cu.getModel('brand');
        const ProductCate = cu.getModel('product_cate');
        const CommodityStock = cu.getModel('commodity_stock');
        const MergeMiniUserProduct = cu.getModel('merge_mini_user_product')

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'brand_id',
                'product_cate_id',
                'preview_pic_url',
                'list_view_pic_url',
                'pic_url',
                'description',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
            ],
        }

        queryParam['offset'] = (param.page - 1) * param.size;
        queryParam['order'] = orderRule;
        queryParam['limit'] = limitRule;
        queryParam['where'] = whereRule;

        try {
            let products = []
            let count = 0
            if (param['user_id']) {
                //查询个人收藏
                MergeMiniUserProduct.belongsTo(Product, { foreignKey: 'product_id', targetKey: 'id' })
                let include = {
                    model: Product,
                    attributes: queryParam.attributes,
                    where: queryParam.whereRule,
                    order: orderRule
                }
                products = await MergeMiniUserProduct.findAndCountAll({
                    attributes: [],
                    include,
                    where: {
                        "mini_user_id": param['user_id'],
                        "lstate": enums.DataLState.ACTIVE,
                        ...whereRule
                    },
                    offset: (param.page - 1) * param.size,
                    limit: limitRule

                })
                count = products.length
                products = products.rows;
                products = JSON.parse(JSON.stringify(products));
                for (let i = 0; i < products.length; i++) {

                    products[i] = products[i]['t_product']
                }
            } else {
                try {
                    products = await Product.findAndCountAll(queryParam);
                } catch (e) {
                    console.log(e)
                }

                count = products.count;
                products = products.rows;
                products = JSON.parse(JSON.stringify(products));
            }

            let saleIds = []
            let productIds = []
            products.map((obj) => {
                obj['pic_url'] = obj['pic_url'].split('|');
                obj['brand_id'] = obj['brand_id'].split('|');
                obj['product_cate_id'] = obj['product_cate_id'].split('|');
                saleIds.push(obj['id'])
                productIds.push(obj['id'])
            });

            let brandMap = {},
                productCateMap = {};

            let brands = await Brand.findAll({
                attributes: [
                    'id',
                    'name',
                    'weight',
                ],
                where: {
                },
            });
            let productCates = await ProductCate.findAll({
                attributes: [
                    'id',
                    'name',
                    'weight',
                ],
                where: {
                },
            });

            brands.map((obj) => {
                brandMap[obj['id']] = obj;
            });
            productCates.map((obj) => {
                productCateMap[obj['id']] = obj;
            });

            // logger.debug(brands);
            // logger.debug(brandMap);



            let relProductStock = {}
            await CommodityStock.findAll({
                attributes: [
                    'id','commodity_id', 'spec_name', 'fee', 'fee_type', 'num'
                ],
                where: {
                    'lstate': enums.DataLState.ACTIVE,
                    'commodity_id': saleIds,
                    'commodity_type': enums.CommodityType.Product
                }
            }).map((obj) => {
                if (!relProductStock[obj.commodity_id]) {
                    relProductStock[obj.commodity_id] = []
                }
                relProductStock[obj.commodity_id].push(obj)
            })


            let CollectedIds = await dataHelper.GetProductCollectedStatus(req.session['userId'],productIds)

            products.map((obj) => {
                obj['product_cate'] = [];
                obj['brand'] = [];

                logger.debug(obj['brand_id']);
                logger.debug(brandMap);

                obj['brand_id'].map((i) => {
                    obj['brand'].push(brandMap[i]);
                });
                obj['product_cate_id'].map((i) => {
                    obj['product_cate'].push(productCateMap[i]);
                });

                delete obj['brand_id'];
                delete obj['product_cate_id'];


                obj['commodity_sale_list'] = []
                if (relProductStock[obj['id']]) {
                    obj['commodity_sale_list'] = relProductStock[obj['id']]
                }

                if (CollectedIds[obj['id']]) {
                    obj['collected_status'] = 1
                } else {
                    obj['collected_status'] = 0
                }

                return obj;
            });

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
                list: products,
                size: products.length,
                page: param.page,
                total_num: count,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },

    getProductDetail: async function (req, res) {
        let logger = req.logger
        let param = req.query;

        try {
            cu.checkFieldExists(param, 'id');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        let whereRule = {};

        whereRule['$t_product.id$'] = {
            '$eq': param['id'],
        }
        whereRule['$t_product.lstate$'] = {
            '$eq': enums.DataLState.ACTIVE,
        }

        const Product = cu.getModel('product');
        const Brand = cu.getModel('brand');
        const ProductCate = cu.getModel('product_cate');
        const CommodityStock = cu.getModel('commodity_stock');

        let queryParam = {
            attributes: [
                'id',
                'name',
                'disp_state',
                'brand_id',
                'product_cate_id',
                'preview_pic_url',
                'pic_url',
                'description',
                'weight',
                'modifier',
                'modify_time',
                'create_time',
                'weight',
            ],
        }

        queryParam['where'] = whereRule;

        try {
            let product = await Product.findAll(queryParam);
            product = JSON.parse(JSON.stringify(product));

            if (product.length < 1) {
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            let data = product[0];
            data['pic_url'] = data['pic_url'].split('|');
            data['product_cate_id'] = data['product_cate_id'].split('|');
            data['brand_id'] = data['brand_id'].split('|');

            data['brand'] = await Brand.findAll({
                attributes: [
                    'id',
                    'name',
                    'weight',
                ],
                where: {
                    'id': data['brand_id'],
                },
            });
            data['product_cate'] = await ProductCate.findAll({
                attributes: [
                    'id',
                    'name',
                    'weight',
                ],
                where: {
                    'id': data['product_cate_id'],
                },
            });

            data['commodity_sale_list'] = []

            await CommodityStock.findAll({
                attributes: [
                    'id','commodity_id', 'preview_pic_url','spec_name', 'fee', 'fee_type', 'num'
                ],
                where: {
                    'lstate': enums.DataLState.ACTIVE,
                    'commodity_id': [data.id],
                    'commodity_type': enums.CommodityType.Product
                }
            }).map((obj) => {
                data['commodity_sale_list'].push(obj)
            })


            let CollectedIds = await dataHelper.GetProductCollectedStatus(req.session['userId'],[data['id']])
            data['collected_status'] = CollectedIds[data['id']]

            delete data['product_cate_id'];
            delete data['brand_id'];

            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;
            return res.json(data);
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    }
};
