const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const request = require("request")
const crypto = require("crypto");
const convert = require("xml-js")

let globalContext = require(APP_BASE_PATH + '/include/global');
module.exports = {
    Unifiedorder,
    Jscode2session,
    getAppSign,
    orderquery,
    RspCodeCheck,
    accountCheck,
    checkSign
}


/**
 * code 换 token接口
 * @param {*} options 
 */
function Jscode2session(options) {

    return new Promise(function (resolve, reject) {
        let appId = globalContext.config['wx']['login']['appId']
        let secretKey = globalContext.config['wx']['login']['WxLoginSecretKey']
        let wxUrl = `https://api.weixin.qq.com/sns/jscode2session?appid=${appId}&secret=${secretKey}&js_code=${options['js_code']}&grant_type=authorization_code`
        console.log(wxUrl)
        request.get(wxUrl, {
            timeout: 5000,
        }, function (error, res, body) {

            if (error) {
                return reject(error)
            }
            if (res.statusCode >= 400) {
                return reject(res.statusCode)
            }
            console.log(body)
            result = JSON.parse(body);
            resolve(result)
        });
    });
}


function getDefaultOpt() {
    let options = {}
    options['appid'] = globalContext.config['wx']['pay']['appId']
    options['mch_id'] = globalContext.config['wx']['pay']['mch_id']
    options['device_info'] = "WEB"
    options['nonce_str'] = `${new Date().getTime()}${Math.ceil(Math.random()*100)}`
    options['sign_type'] = "MD5"
    return options
}
function checkSign(body){
    let sign = body['sign']

    delete body.sign

    let iSign = wxSign(body, globalContext.config['wx']['pay']['key'])

    if(sign == iSign){
        return true
    }
    throw new Error("wx sign failed ")
}

function RspCodeCheck(body){
    if(body['return_code'] == "SUCCESS"){
        return true
    }
    throw new Error("wx failed "+body['return_code'])
}

function accountCheck(body){
    if(body['appid'] == globalContext.config['wx']['pay']['appId'] && body['mch_id'] == globalContext.config['wx']['pay']['mch_id']){
        return true
    }
    throw new Error("wx failed ,not our account")
}

/**
 * 微信支付统一下单接口
 * @param {*} options 
 */
function Unifiedorder(options) {

    options = {
        ...options,
        ...getDefaultOpt()
    }

    let sign = wxSign(options, globalContext.config['wx']['pay']['key'])

    options['sign'] = sign
    let xmlData = convert.js2xml({xml:options}, { compact: true, spaces: 4 })
    var rOpts = {
        method: 'POST',
        url: 'https://api.mch.weixin.qq.com/pay/unifiedorder',
        headers:
        {
            'content-type': 'application/xml'
        },
        timeout:5 * 1000,
        body: xmlData
    };

    console.log(rOpts)


    return new Promise(function(resolve,reject){
        request(rOpts, function (error, response, body) {
           
            if (error){
                return reject(new Error(error));
            } 
            if(response.statusCode >= 400){
                return reject(new Error("net error"))
                
            }
          
            body = convert.xml2js(body,{compact:true,spaces:4})
            body = body['xml']

            if(!body){
                return reject(new Error("net error"))
            }
            for(let key in body){
                console.log(key)
                body[key] = body[key]['_cdata']
            }
            return resolve(body)
        });
    })
}

/**
 * 微信支付 订单查询接口
 * @param {*} options 
 */
async function orderquery(options) {


    options = {
        ...options,
        ...getDefaultOpt()
    }

    let sign = wxSign(options, globalContext.config['wx']['pay']['key'])

    options['sign'] = sign
    let xmlData = convert.js2xml({xml:options}, { compact: true, spaces: 4 })
    var rOpts = {
        method: 'POST',
        url: 'https://api.mch.weixin.qq.com/pay/orderquery',
        headers:
        {
            'content-type': 'application/xml'
        },
        timeout:5 * 1000,
        body: xmlData
    };

    console.log(rOpts)

    return new Promise(function(resolve,reject){
        request(rOpts, function (error, response, body) {
           
            if (error){
                return reject(new Error(error));
            } 
            if(response.statusCode >= 400){
                return reject(new Error("net error"))
                
            }
          
            body = convert.xml2js(body,{compact:true,spaces:4})
            body = body['xml']

            if(!body){
                return reject(new Error("net error"))
            }
            for(let key in body){
                console.log(key)
                body[key] = body[key]['_cdata']
            }
            return resolve(body)
        });
    })
}


/**
 * 获取app小程序发起的
 * @param {*} options 
 */
function getAppSign(options) {
    let sign = wxSign(options,globalContext.config['wx']['pay']['key'])
    return {
        ...options,
        sign
    }
}


function wxSign(body, skey) {
    let strs = []
    for (let key in body) {
        if (body[key]) {
            strs.push(`${key}=${body[key]}`)
        }
    }
    strs.sort(function (a, b) {
        return a < b ? -1 : 1
    })
    strA = strs.join("&")
    tempStr = `${strA}&key=${skey}`
    console.log(tempStr)
    const md5 = crypto.createHash('md5');
    md5.update(tempStr);
    sign = md5.digest('hex');
    body['sign'] = sign.toUpperCase()
    return sign.toUpperCase()
}