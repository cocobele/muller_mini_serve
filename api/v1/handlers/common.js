
const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const globalContext = require(APP_BASE_PATH + 'include/global');
const request = require('request-promise-native');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');
const Core = require('@alicloud/pop-core');
const wx = require('./wx')

module.exports = {
    getConfig:async function (req,res){

        const AppConfig = cu.getModel('app_config');

        let configs = await AppConfig.findAll({
            attributes:["id","val"],
            where:{
                "lstate":1
            }
        })

        configs = JSON.parse(JSON.stringify(configs));
        if (configs.length < 1) {
            return res.json({
                ret_code: ec.RES_NOT_EXISTS.code,
                ret_msg: ec.RES_NOT_EXISTS.message,
            });
        }

        let data = {}
        let app = {}
        for(let c of configs){
            data[c['id']] = c['val']

            if(c['id']=="artist_id_liyanxun"){
                app['admin_artist_id'] = c['val']
            }
        }
        data['app'] = app
        data['ret_code'] = ec.SUCCESS.code;
        data['ret_msg'] = ec.SUCCESS.message;
        return res.json(data);

    },
    getAuthSession: async function (req, res) {
        let logger = req.logger
        let param = req.query;
        try {
            cu.checkFieldExists(param, 'js_code');

        } catch (e) {
            logger.printExecption(e);

            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        //查看是否是合法微信用户
        const MiniUser = cu.getModel('mini_user');
        let transaction = null;
        try {

            let result = await wx.Jscode2session(param)
            if (result.errcode != 0 && result.errcode != undefined) {
                if (!req.session) {
                    req.session = {};
                }
                req.session['debug_info'] = "js_code_invalid"
                logger.printExecption(JSON.stringify(result));
                return res.json({
                    ret_code: ec.LOGIN_CODE_INVALID.code,
                    ret_msg: ec.LOGIN_CODE_INVALID.message,
                });

            }

            if (!req.session) {
                req.session = {};
            }

            //更新会话
            req.session.openId = result.openid;
            param['id'] = uuidv1().replace(/-/g, '');
            param['open_id'] = result.openid
            param['modify_time'] = Date.now();
            param['create_time'] = Date.now();

            transaction = await globalContext.database['mysql']['db_muller'].transaction();
            let fondItem = await MiniUser.findOne({attributes:["id"], where: { "open_id": { "$eq": result.openid } } ,transaction:transaction})

            if (!fondItem) {
                await MiniUser.create(param, { transaction: transaction })
            } else {

                //await MiniUser.update({ "last_login_time": Date.now() }, { where: { "open_id": { "$eq": result.openid } } ,transaction:transaction})

            }
            fondItem = await MiniUser.findOne({attributes:["id","open_id"], where: { "open_id": { "$eq": result.openid } },transaction:transaction })
            req.session.userId = fondItem['id'];
            req.session.openId = fondItem['open_id']
            console.log(req.session)
            await transaction.commit()

            //更新用户登陆时间
            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });

        } catch (e) {
            if (transaction) {
                await transaction.rollback();
            }
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    }
}
