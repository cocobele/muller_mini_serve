const APP_BASE_PATH = '../../../';
const logger = require(APP_BASE_PATH + 'utils/logger');
const _ = require('lodash');
const enums = require(APP_BASE_PATH + 'include/enum');
const ec = require(APP_BASE_PATH + 'include/errcode');
const cu = require(APP_BASE_PATH + 'utils/common');
const ci = require(APP_BASE_PATH + 'include/common');
const request = require('request-promise-native');
const Sequelize = require('sequelize');
const moment = require('moment');
const uuidv1 = require('uuid/v1');
const Core = require('@alicloud/pop-core');
const wx = require("./wx")
const cache = require(APP_BASE_PATH + "include/cache")
const dataHelper = require("./data")

module.exports = {

    //收藏或者取消收藏
    collectExhibition: async function (req, res) {

        //check the param
        let logger = req.logger
        let param = req.body;
        try {
            cu.checkFieldExists(param, 'exhibition_id');
            cu.checkFieldExists(param, 'opt');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        //check the data exist
        let Exhibition = cu.getModel('exhibition')
        let MergeMiniUserExhibiton = cu.getModel('merge_mini_user_exhibition')

        let queryParam = {
            attributes: [
                'id'
            ],
        }

        queryParam['where'] = {
            "lstate": {
                '$eq': enums.DataLState.ACTIVE
            },
            "id": param['exhibition_id']
        };

        try {

            let exhibitions = await Exhibition.findAll(queryParam);
            exhibitions = JSON.parse(JSON.stringify(exhibitions));
            if (exhibitions.length < 1) {
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            param['mini_user_id'] = req.session['userId']
            param['modifier'] = ci.DEFAULT_MODIFIER
            param['modify_time'] = Date.now()
            param['create_time'] = Date.now()
            if (param['opt'] == 1) {
                param['lstate'] = enums.DataLState.ACTIVE
            } else {
                param['lstate'] = enums.DataLState.DELETED
            }

            await MergeMiniUserExhibiton.upsert(param)
            cache.DelPersonal(req.session['userId'])

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });

        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });

        }
    },
    collectArtwork: async function (req, res) {


        //check the param
        let logger = req.logger
        let param = req.body;
        try {
            cu.checkFieldExists(param, 'artwork_id');
            cu.checkFieldExists(param, 'opt');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        //check the data exist
        let Artwork = cu.getModel('artwork')
        let MergeMiniUserArtwork = cu.getModel('merge_mini_user_artwork')

        let queryParam = {
            attributes: [
                'id'
            ],
        }

        queryParam['where'] = {
            "lstate": enums.DataLState.ACTIVE,
            "id": param['artwork_id']
        };

        try {

            let artworks = await Artwork.findAll(queryParam);
            artworks = JSON.parse(JSON.stringify(artworks));
            if (artworks.length < 1) {
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            param['mini_user_id'] = req.session['userId']
            param['modifier'] = ci.DEFAULT_MODIFIER
            param['modify_time'] = Date.now()
            param['create_time'] = Date.now()
            if (param['opt'] == 1) {
                param['lstate'] = enums.DataLState.ACTIVE
            } else {
                param['lstate'] = enums.DataLState.DELETED
            }

            await MergeMiniUserArtwork.upsert(param)
            cache.DelPersonal(req.session['userId'])

            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });

        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });

        }
    },
    collectProduct: async function (req, res) {

        //check the param
        let logger = req.logger
        let param = req.body;
        try {
            cu.checkFieldExists(param, 'product_id');
            cu.checkFieldExists(param, 'opt');
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }

        //check the data exist
        let Product = cu.getModel('product')
        let MergeMiniUserProduct = cu.getModel('merge_mini_user_product')

        let queryParam = {
            attributes: [
                'id'
            ],
        }

        queryParam['where'] = {
            "lstate": enums.DataLState.ACTIVE,
            "id": param['product_id']
        };

        try {

            let products = await Product.findAll(queryParam);
            products = JSON.parse(JSON.stringify(products));
            if (products.length < 1) {
                return res.json({
                    ret_code: ec.RES_NOT_EXISTS.code,
                    ret_msg: ec.RES_NOT_EXISTS.message,
                });
            }

            param['mini_user_id'] = req.session['userId']
            param['modifier'] = ci.DEFAULT_MODIFIER
            param['modify_time'] = Date.now()
            param['create_time'] = Date.now()
            if (param['opt'] == 1) {
                param['lstate'] = enums.DataLState.ACTIVE
            } else {
                param['lstate'] = enums.DataLState.DELETED
            }

            await MergeMiniUserProduct.upsert(param)
            cache.DelPersonal(req.session['userId'])
            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });

        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },
    getPersonal: async function (req, res) {
        let logger = req.logger
        try {
            try{
                let obj = await cache.GetPersonal(req.session['userId'])
                if(obj){
                    obj['ret_code'] = ec.SUCCESS.code;
                    obj['ret_msg'] = ec.SUCCESS.message;
                    return res.json(obj);
                }

            }catch(e){}
            let data = await dataHelper.GetPersonal(req.session['userId'], req.session['openId'])
            cache.SetPersonal(req.session['userId'],data)
            data['ret_code'] = ec.SUCCESS.code;
            data['ret_msg'] = ec.SUCCESS.message;

            return res.json(data);

        } catch (e) {

            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    },
    editPersonal: async function (req, res) {

        let logger = req.logger
        let param = req.body
        try {
            cu.checkFieldExists(param, 'name');
            cu.checkFieldExists(param, 'email');
            cu.checkFieldExists(param, 'phone');
            cu.checkFieldExists(param, 'address');
         

        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.PARAM_ERROR.code,
                ret_msg: e.message || ec.PARAM_ERROR.message,
            });
        }
        const MiniUser = cu.getModel('mini_user');
        try {
            await MiniUser.update(param, {
                where: {
                    id: req.session['userId']
                }
            })
            cache.DelPersonal(req.session['userId'])
            return res.json({
                ret_code: ec.SUCCESS.code,
                ret_msg: ec.SUCCESS.message,
            });
        } catch (e) {
            logger.printExecption(e);
            return res.json({
                ret_code: ec.FAILED.code,
                ret_msg: ec.FAILED.message,
            });
        }
    }
}
