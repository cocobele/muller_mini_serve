const APP_BASE_PATH = '../../';
const express = require('express');
const router = express.Router();

router.use('/mini_app', require('./routes/common'));

router.use('/mini_app/common', require('./routes/common'));

router.use('/mini_app/callback', require('./routes/callback'));




// 是否是调试后门请求，配置文件控制是否启用
router.use(require(APP_BASE_PATH + 'middleware/auth').debugValidate);
router.use(require(APP_BASE_PATH + 'middleware/auth').signValidate);

// 检查微信登陆状态
router.use(require(APP_BASE_PATH + 'middleware/auth').sessionValidate);


router.use('/mini_app', require('./routes/content'));


router.use('/mini_app/personal', require('./routes/personal'));

router.use('/mini_app/order', require('./routes/order'));




module.exports = router;