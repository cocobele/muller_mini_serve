
const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/callback');
const xmlparser = require('express-xml-bodyparser');


router.post('/wx_pay_msg', xmlparser({ trim: false, explicitArray: false }), function (req, res, next) {
    handler.wxPayMsg(req, res)
});

module.exports = router;

