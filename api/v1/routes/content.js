
const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/content');


router.get('/get_brand_list', function (req, res) {
    handler.getBrandList(req, res)
});

router.get('/get_brand_detail', function (req, res) {
    handler.getBrandDetail(req, res)
});

router.get('/get_exhibition_list', function (req, res) {
    handler.getExhibitionList(req, res)
});

router.get('/get_exhibition_detail', function (req, res) {
    handler.getExhibitionDetail(req, res)
});

router.get('/get_article_list', function (req, res) {
    handler.getArticleList(req, res)
});

router.get('/get_article_detail', function (req, res) {
    handler.getArticleDetail(req, res)
});

router.get('/get_artwork_list', function (req, res) {
    handler.getArtworkList(req, res)
});

router.get('/get_artwork_detail', function (req, res) {
    handler.getArtworkDetail(req, res)
});


router.get('/get_product_cate_list', function (req, res) {
    handler.getProductCateList(req, res)
});


router.get('/get_artist_detail', function (req, res) {
    handler.getArtistDetail(req, res)
});

router.get('/get_artist_list',function(req,res){
    handler.getArtistList(req,res)
})


router.get('/get_product_list',function(req,res){
    handler.getProductList(req,res)
})


router.get('/get_product_detail',function(req,res){
    handler.getProductDetail(req,res)
})


module.exports = router;

