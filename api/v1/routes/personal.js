
const express = require('express');
const router = express.Router();
const contentHandler = require('../handlers/content');
const handler = require('../handlers/personal');

router.get('/get_exhibition_list', function (req, res) {
    if(!req.query){
        req.query = {}
    }
    req.query['user_id'] = req.session.userId
    contentHandler.getExhibitionList(req, res)
});

router.get('/get_artwork_list', function (req, res) {
    if(!req.query){
        req.query = {}
    }
    req.query['user_id'] = req.session.userId
    contentHandler.getArtworkList(req, res)
});

router.get('/get_product_list',function(req,res){
    if(!req.query){
        req.query = {}
    }
    req.query['user_id'] = req.session.userId
    contentHandler.getProductList(req,res)
})

router.post('/collect_exhibition',function(req,res){
    handler.collectExhibition(req,res)
})


router.post('/collect_artwork',function(req,res){
    handler.collectArtwork(req,res)
})


router.post('/collect_product',function(req,res){
    handler.collectProduct(req,res)
})

router.post('/edit_personal',function(req,res){
    handler.editPersonal(req,res)
})


router.get('/get_personal',function(req,res){
    handler.getPersonal(req,res)
})
module.exports = router;

