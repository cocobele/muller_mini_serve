
const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/order');


router.post('/create_order',function(req,res){
    handler.CreateOrder(req,res)
})

router.post('/repay_order',function(req,res){
    handler.RepayOrder(req,res)
})

router.get('/get_b2c_order_list',function(req,res){
    handler.getB2cOrderList(req,res)
})

router.get('/get_b2c_order_detail',function(req,res){
    handler.getB2cOrderDetail(req,res)
})

router.post('/get_shoppingcart_detail',function(req,res){
    handler.getShoppingcartDetail(req,res)
})


module.exports = router;

