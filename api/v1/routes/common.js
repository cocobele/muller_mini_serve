
const APP_BASE_PATH = '../../../';
const express = require('express');
const router = express.Router();
const handler = require('../handlers/common');


// 获取登陆session
router.get('/get_auth_session', function (req, res) {
    handler.getAuthSession(req, res)
})

router.get('/get_config', function (req, res) {
    handler.getConfig(req, res)
});



module.exports = router;

