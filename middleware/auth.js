const logger = require('../utils/logger');
const _ = require('lodash');
const enums = require('./../include/enum');
const cu = require('../utils/common');
const ec = require('../include/errcode');
const request = require('request-promise-native');
const lru = require('lru-cache');
let globalContext = require('../include/global');


module.exports = {


    debugValidate:async function(req,res,next){
        if(req.headers){
            if(globalContext.config['enable_debug_validate'] == true && req.headers['debug'] == globalContext.config['debug_validate']){
               
                if(!req.session){
                    req.session = {}
                }
                req.session['userId'] = '210d4e14ff2f11e9a2ceacde48001122'
                req.session['openId'] = "1234"
            }
        }
        next()
    },


    signValidate:async function(req,res,next){

        if(globalContext.config['enable_sign_validate'] != true){
            return next()
        }
        let timeStamp = req.query['timestamp']
        let url = req.originalUrl.split("?")[0]

    
        let reqSign = req.query['sign']
        if(timeStamp == undefined || reqSign == undefined) {
            return next()
        }

        timeStamp = parseInt(timeStamp)


        let nTimeStamp = new Date().getTime()
        if (nTimeStamp - timeStamp > 1000 * 60){
            return next()
        }
    
        if(globalContext.config['debug_sign'] == undefined){
            return next()
        }
    
    
        let sign = cu.sha256(url+timeStamp+globalContext.config['debug_sign'])
        if(sign == reqSign) {
            if(req.session){
                req.session['userId'] = '210d4e14ff2f11e9a2ceacde48001122'
                req.session['openId'] = "1234"
            }
           
        }
        return next()

    },


    sessionValidate: async function (req, res, next) {
        let tNowSec = Math.ceil(new Date().getTime() / 1000),
            session = req.session;
        //检查登录
        if (!session || !session.openId || !session.userId) {
            return res.status(401).json({
                request_id:req.request_id,
                ret_code: ec.NOT_LOGGED_IN.code,
                ret_msg: ec.NOT_LOGGED_IN.message,
            });
        }

        // 是否会话过期
        if (tNowSec > session.expireIn) {
            return res.status(401).json({
                request_id:req.request_id,
                ret_code: ec.USER_SESSION_EXPIRED.code,
                ret_msg: ec.USER_SESSION_EXPIRED.message,
            });
        }
        req.logger.info(session)
        next();
    },

};
