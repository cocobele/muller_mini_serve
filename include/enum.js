var _enumValidator = {}


var TRADE_STATE = {
    "SUCCESS":1,
    "REFUND":23,
    "NOTPAY":0,
    "CLOSED":22,
    "REVOKED":0,
    "USERPAYING":0,
    "PAYERROR":21
}


var ENUM = {

    // 数据状态
    DataLState: {
        ACTIVE: 1,
        DELETED: 0,
        INACTIVE:2,
    },
    CommodityType:{
        ArtWork:1,
        Product:2
    },
    CollectedType:{
        Collected:1,
        UnCollected:0
    },
    PayChannelType:{
        WxPay:1
    },
    OrderTradeState:{
        NotPay:0,
        NotExist:24,
        Expire:25
    },
    TRADE_STATE
};




ENUM.validateEnum = function(enumName, enumVal){
    if(typeof ENUM[enumName] == 'undefined'){
        throw 'Enum ' + enumName + ' not exists';
    }

    if(!_enumValidator[enumName]){
        _enumValidator[enumName] = {};
        for(var k in ENUM[enumName]){
            _enumValidator[enumName][ENUM[enumName][k]] = true;
        }
    }

    return _enumValidator[enumName][enumVal] === true;
}

module.exports = ENUM;