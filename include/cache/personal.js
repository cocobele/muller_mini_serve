


let globalContext = require('../global');
module.exports = {
    GetPersonal,
    SetPersonal,
    DelPersonal
}


function GetPersonalKey(id){
    return `user:${id}`
}

function GetPersonal(id){
    return new Promise(function(resolve,reject){
        let client = globalContext.database['redis']['content']
        client.get(GetPersonalKey(id), function (err, str) {
            if(err){
                return reject(err)
            } else {
                try{
                    return resolve(JSON.parse(str))

                }catch(e){
                    return reject(e)

                }   
            }
        });
    })
}


function SetPersonal(id,data){
    return new Promise(function(resolve,reject){
        let client = globalContext.database['redis']['content']
        let key  = GetPersonalKey(id)

        let str = JSON.stringify(data)
        client.set(key, str,function(err){
            if(err){
                return reject(err)
            }
            return resolve()
        });
    })
}

function DelPersonal(id){
    return new Promise(function(resolve,reject){
        let client = globalContext.database['redis']['content']
        client.del(GetPersonalKey(id), function (err, obj) {
            if(err){
                return reject(err)
            } else {
                return resolve(obj)
            }
        });

    });
}