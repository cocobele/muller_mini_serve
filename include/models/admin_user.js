
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_admin_user', {
        id: {
            autoIncrement: true,
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING(64),
        },
        portrait_url: {
            type: DataTypes.STRING(256),
        },
        gender: {
            type: DataTypes.INTEGER,
        },
        wechat_id: {
            type: DataTypes.STRING(64),
        },
        phone: {
            type: DataTypes.STRING(64),
        },
        password: {
            type: DataTypes.STRING(256),
        },
        enabled: {
            type: DataTypes.INTEGER,
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
