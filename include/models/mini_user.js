
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_mini_user', {
        id: {
            autoIncrement: true,
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        open_id: {
            type: DataTypes.STRING(256),
        },
        name: {
            type: DataTypes.STRING(128),
        },
        email: {
            type: DataTypes.STRING(128),
        },
        address: {
            type: DataTypes.STRING(256),
        },
        phone: {
            type: DataTypes.STRING(128),
        },
        logo: {
            type: DataTypes.STRING(256),
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        login_time: {
            type: DataTypes.DATE(6)
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
