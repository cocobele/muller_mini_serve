
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_b2c_order_roll', {
        b2c_order_id: {
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        old_trade_state: {
            type: DataTypes.INTEGER,
        },
        trade_state: {
            type: DataTypes.INTEGER,
        },
        trade_state_desc: {
            type: DataTypes.STRING(256),
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
