module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_image', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER,
            primaryKey: true
        },
        mime: {
            type: DataTypes.STRING(16),
        },
        height: {
            type: DataTypes.INTEGER,
        },
        width: {
            type: DataTypes.INTEGER,
        },
        file_size: {
            type: DataTypes.INTEGER,
        },
        raw_url: {
            type: DataTypes.STRING(256),
        },
        file_url: {
            type: DataTypes.STRING(256),
        },
        modifier: {
            type: DataTypes.STRING(100),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
            primaryKey: true
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
