
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_order_stock', {
        id: {
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        stock_id: {
            type: DataTypes.STRING(64),
        },
        order_id: {
            type: DataTypes.STRING(64),
        },
        spec_name: {
            type: DataTypes.STRING(256),
        },
        preview_pic_url:{
            type: DataTypes.STRING(128),
        },
        name: {
            type: DataTypes.STRING(128),
        },
        fee: {
            type: DataTypes.INTEGER,
        },
        fee_type: {
            type: DataTypes.STRING(16),
        },
        num: {
            type: DataTypes.INTEGER,
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
