
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_app_config', {
        id: {
            autoIncrement: true,
            type: DataTypes.STRING(64),
            primaryKey: true
        },
        val: {
            type: DataTypes.STRING(64),
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        modify_time: {
            type: DataTypes.DATE(6)
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
