
module.exports = (sequelize, DataTypes) => {

    return sequelize.define('t_audio', {
        id: {
            autoIncrement: true,
            type: DataTypes.INTEGER,
            primaryKey: true,
        },
        mime: {
            type: DataTypes.STRING(16),
        },
        audio_length: {
            type: DataTypes.INTEGER,
        },
        file_size: {
            type: DataTypes.INTEGER,
        },
        raw_url: {
            type: DataTypes.STRING(256),
        },
        file_url: {
            type: DataTypes.STRING(256),
        },
        lstate: {
            type: DataTypes.INTEGER,
        },
        modifier: {
            type: DataTypes.STRING(16),
        },
        modify_time: {
            type: DataTypes.DATE(6),
        },
        create_time: {
            type: DataTypes.DATE(6),
        },
    }, {
        freezeTableName: true,
        timestamps: false,
    })

};
