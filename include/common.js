const logger = require('../utils/logger');
const enums = require('./../include/enum');


var MODULE = {};

MODULE.updateOrCreate = async function(model, where, newItem,updateItem) {
    // First try to find the record
    let fondItem = await model.findOne({where:where})
    if(fondItem){
        let item = await model.create(newItem)
        return {item,created:true}
    } else {
        let item = await model.update(updateItem,{where:where})
        return {item,created:false}
    }
}


MODULE.handlePageParam = function(param, defaultPageSize) {
    const MAX_PAGE_SIZE = 100;

    param.size = parseInt(param.size);

    if(!param.size){
        if(defaultPageSize){
            param.size = defaultPageSize;
        } else {
            param.size = 50;
        }
    }

    if(param.size > MAX_PAGE_SIZE){
        param.size = MAX_PAGE_SIZE;
    }

    if(!param.page || param.page < 1){
        param.page = 1;
    }
}

// 默认修改人
MODULE.DEFAULT_MODIFIER = 'admin';

module.exports = MODULE;