const personal = require("./cache/personal");


module.exports = {
    GetPersonal:personal.GetPersonal,
    SetPersonal:personal.SetPersonal,
    DelPersonal:personal.DelPersonal
}