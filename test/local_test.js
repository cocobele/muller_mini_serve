const request = require("request")
const crypto = require("crypto")


function sha256(str){
    let sha256 = crypto.createHash('sha256');
    return sha256.update(str).digest("hex");
}


function getSign(url){

    let timestamp =  new Date().getTime();
    let key = "6b9d1a00f03d11e98b23cd7a39219aeb"

    let sign = sha256( url + timestamp+ key)


    url += "?timestamp="+timestamp
    //url += "&sign="+sign

    return url
}

//let endPoint = "yanshan-dev.chdtrip.com";
let endPoint = "127.0.0.1:8081";
let protocal = "http://"


async function asyncRequest(opts){
    request(opts,function(error,rsp,body){

        console.log("------------------------------------start:"+opts.url+"-----------------------------------")
        console.log("error:",error)
        if(!error){
            console.log(JSON.parse(body))

        }

        console.log("------------------------------------end------------------------")
        console.log()


    })
}


async function testGetArticleDetail(){

    let url = "/api/v1/mini_app/get_artist_detail"
    url = getSign(url)
    url += "&id=d9fb6b50f22711e9972163a9fef42d21"
    let opts = {
        method:"GET",
        url:protocal + endPoint + url,
        timeout:3000

    }
    await asyncRequest(opts)
}


async function testGetArticleList(){

    let url = "/api/v1/mini_app/get_article_list"
    url = getSign(url)
    let opts = {
        method:"GET",
        url:protocal + endPoint + url,
        timeout:3000

    }
    await asyncRequest(opts)
}

async function testGetArticleDetail(){

    let url = "/api/v1/mini_app/get_article_detail"
    url = getSign(url)
    url += "&id=123"
    let opts = {
        method:"GET",
        url:protocal + endPoint + url,
        timeout:3000

    }
    await asyncRequest(opts)
}


async function testGetProductCateList(){

    let url = "/api/v1/mini_app/get_product_cate_list"
    url = getSign(url)
    url += "&id=123"
    let opts = {
        method:"GET",
        url:protocal + endPoint + url,
        timeout:3000

    }
    await asyncRequest(opts)
}



async function testGetArtistDetail(){

    let url = "/api/v1/mini_app/get_artist_detail"
    url = getSign(url)
    url += "&id=123"
    let opts = {
        method:"GET",
        url:protocal + endPoint + url,
        timeout:3000

    }
    await asyncRequest(opts)
}


async function testGetArtworkDetail(){

    let url = "/api/v1/mini_app/get_artwork_detail"
    url = getSign(url)
    url += "&id=b198b2f0f22f11e9a93b2dac48a506ab"
    let opts = {
        method:"GET",
        url:protocal + endPoint + url,
        timeout:3000

    }
    await asyncRequest(opts)
}

async function testGetArtworkList(){

    let url = "/api/v1/mini_app/get_artwork_list"
    url = getSign(url)
    url += "&id=123"
    let opts = {
        method:"GET",
        url:protocal + endPoint + url,
        timeout:3000

    }
    await asyncRequest(opts)
}

async function testGetExhibitionList(){

    let url = "/api/v1/mini_app/get_exhibition_list"
    url = getSign(url)
    url += "&id=7f395ee0f0f911e9ac7915cbd5791fe0"
    let opts = {
        method:"GET",
        url:protocal + endPoint + url,
        timeout:3000

    }
    await asyncRequest(opts)
}


async function testGetExhibitionDetail(){

    let url = "/api/v1/mini_app/get_exhibition_detail"
    url = getSign(url)
    url += "&id=7f395ee0f0f911e9ac7915cbd5791fe0"
    let opts = {
        method:"GET",
        url:protocal + endPoint + url,
        timeout:3000

    }
    await asyncRequest(opts)
}


async function testGetBrandDetail(){

    let url = "/api/v1/mini_app/get_brand_detail"
    url = getSign(url)
    url += "&id=123"
    let opts = {
        method:"GET",
        url:protocal + endPoint + url,
        timeout:3000

    }
    await asyncRequest(opts)
}

async function testGetBrandList(){

    let url = "/api/v1/mini_app/get_brand_list"
    url = getSign(url)
    url += "&id=123"
    let opts = {
        method:"GET",
        url:protocal + endPoint + url,
        timeout:3000

    }
    await asyncRequest(opts)
}

(async function(){

    //await testGetArticleDetail()
    //await testGetArticleList()
    // await testGetProductCateList()
    // await testGetArtistDetail()
     await testGetArtworkDetail()
     await testGetArtworkList()
     //await testGetExhibitionList()
    // await testGetExhibitionDetail()
    // await testGetBrandDetail()
    // await testGetBrandList()

})()



